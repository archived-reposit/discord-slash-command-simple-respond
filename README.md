# Simple Discord slash command

Responds with a phrase when called. Typescript.

## Background

Unexpectedly I ended up setting up a Discord-server, and naturally created a bot. But bots are like normal users and have to be online to be functional (expensive or at least unpractical). So I ended up using the recently added slash commands. These can be used with http-endpoints and are thus suitable for deploying to aws-lambda etc.

[docs](https://discord.com/developers/docs/interactions/slash-commands)

## Register command

Registering a slash-command is done with a post request. Here that is done with the python-script in [register-command](/register-command). I ended up using curl instead, easier when modifying.

## The response

The responses are read from /src/phrases.ts, one is randomly selected and sent as the response. Rename [phrases.example.ts](/src/phrases.example.ts) to src/phrases.ts.
There are some security checks that are handled in [middleware.ts](/src/middleware.ts). Rename [public-key.example.json](/src/public_key.example.json) to src/public-key.json and add your key.

## Deploying

Serverless is used for deploying to an endpoint that triggers aws-lambda. The endpoint must then be saved in the se
ttings for the discord app. When saving the endpoint, requests are made that have to be responded to according to the docs.
