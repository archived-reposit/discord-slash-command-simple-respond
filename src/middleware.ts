import * as nacl from "tweetnacl";
import express = require("express");
import * as public_key from "./public_key.json";
import { phrases } from "./phrases"; // string[]
export function middleWare(req: express.Request, res: express.Response) {
  console.log(JSON.stringify(req.headers));
  if (!verify(req)) {
    console.log("not verified");
    return res.status(401).end("invalid request signature");
  }
  // check if it is a test, a ping sent when saving the endpoint
  if (isPing(req)) {
    console.log("is ping");
    return res.status(200).end(JSON.stringify({ type: 1 }));
  }
  let s = getPhrase();
  if ((req.body as any).type != null && (req.body as any).type === 2) {
    const resp = {
      type: 4,
      data: {
        tts: false,
        content: s,
        embeds: [],
        allowed_mentions: { parse: [] },
      },
    };
    return res.status(200).end(JSON.stringify(resp));
  }
}

function getPhrase(): string {
  let x = Math.floor(Math.random() * phrases.length);
  return phrases[x];
}

function verify(req: express.Request): boolean {
  const PUBLIC_KEY = public_key.pk;
  const signature = req.get("X-Signature-Ed25519");
  const timestamp = req.get("X-Signature-Timestamp");
  const body = (req as any).rawBody.toString(); // rawBody is added manually in ind
  console.log("body " + JSON.stringify(body));
  let isVerified: boolean;
  try {
    isVerified = nacl.sign.detached.verify(
      Buffer.from(timestamp + body),
      Buffer.from(signature, "hex"),
      Buffer.from(PUBLIC_KEY, "hex")
    );
  } catch (e) {
    console.log("error when verifying: " + e);
    isVerified = false;
  }
  return isVerified;
}

function isPing(req: express.Request): boolean {
  return (req.body as any).type != null && (req.body as any).type === 1;
}
